angular.module('app.main', []);

angular.module('app.main')
    .factory('Config', ['$http', function($http) {
        const config =
            $http.get('http://' + window.location.host + '/config.json')
            .then(function(res) {
                res.data.host = window.location.hostname + ':' + res.data.port;
                console.log(res.data.host);
                return res.data;
            });

        return {
            get : config
        }
    }])
    .factory('Category', ['$http', 'Config', function($http, Config) {
        return {
            all : function () {
                return Config.get.then(config => {
                    return $http.get('http://' + config.host + '/categories')
                        .then(function (res) {
                            var data = res.data;
                            var categories = data.categories;
                            return categories;
                        });
                });
            }
        }
    }])
    .factory('Product', ['$http', 'Config', function($http, Config) {
        return {
            all : function(categoryId, page) {
                var p = page || 1;
                return Config.get.then(config => {
                    return $http.get('http://' + config.host + '/categories/' +
                        categoryId + '/products?page=' + p )
                        .then(function (res) {
                            var data = res.data;
                            return {
                                products: data.products,
                                total : data.total
                            };
                        });
                });
            },
            get : function(productId) {
                return Config.get.then(config => {
                    return $http.get('http://' + config.host + '/products/' + productId )
                        .then(function (res) {
                            var data = res.data;
                            var product = data.product;
                            return product;
                        });
                });
            },
            top : function() {
                return Config.get.then(config => {
                    return $http.get('http://' + config.host + '/top-products')
                        .then(function (res) {
                            var data = res.data;
                            return data.products;
                        });
                });
            }
        }
    }])
    .controller('ProductListCtrl', ['Product', '$stateParams',
        function(Product, $stateParams) {
        const self = this;
        self.products = [];
        self.category = $stateParams['category'];
        self.pages = [];
        loadProducts();

        function loadProducts() {
            Product.all($stateParams['category'], $stateParams['page'])
                .then(function(data) {
                    self.products = data.products;
                    self.total = data.total;
                    var pages = [];
                    for(var i =0; i<Math.ceil(self.total / 3); ++i)
                        pages.push(i+1);
                    self.pages = pages;
                });
        }
    }])
    .controller('TopProductsCtrl', ['Product', function(Product) {
        const self = this;
        self.topProducts = [];

        loadTopProducts();

        function loadTopProducts() {
            Product.top()
                .then(function(top) {
                    self.topProducts = top;
                });
        }
    }])
    .controller('ListCtrl', ['Category', 'Product', function(Category, Product) {
        const self = this;
        self.categories = [];

        loadCategories();

        function loadCategories() {
            Category.all()
                .then(function(categories) {
                   self.categories = categories;
                });
        }
    }]);


angular.module('app', ['app.main', 'ui.router'])
    .config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider.state('home', {
            url : '/home',
            templateUrl : 'views/home.partial.html',
            controller : 'TopProductsCtrl',
            controllerAs : 'vm'
        });

        $stateProvider.state('list', {
            url : '/list',
            templateUrl : 'views/list.partial.html',
            controller : 'ListCtrl',
            controllerAs : 'vm'
        });

        $stateProvider.state('list.products', {
            url : '/list/:category/products/:page?',
            templateUrl : 'views/products.partial.html',
            controller : 'ProductListCtrl',
            controllerAs : 'vm'
        })
    }]);