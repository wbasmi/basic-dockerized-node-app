'use strict';

const express = require('express');
const router = express.Router();
const models = require('../models');

router.get('/categories', (req, res, next) => {
   models.categories.findAll()
       .then(categories => {
           res.status(200)
               .json({
                   categories : categories
               });
       })
});

router.post('/categories/:id/products', (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;
    const price = +req.body.price;


    if(!name || !price)
    {
        res.status(401)
            .json({
                message :'invalid name or price'
            });
        return;
    }

    models.products.create({
        name : name,
        description : description,
        price : price
    }).then(product => {
        models.categories.findOne({
            where : { id : id}
        }).then(category => {
            category.addProduct(product);
        }).then(ok => {
            res.status(200)
                .json({
                    product: product
                })
        });
    })
});

router.get('/products/:id', (req, res, next) => {
   const id = +req.params.id;

   models.products.findOne({
       where : { id : id}
   })
       .then(product => {
           if(product)
               res.status(200)
               .json({
                   product : product
               })
           else res.status(404)
               .json({
                   message : 'product not found !'
               });
       })
});

router.get('/categories/:id/products', (req, res, next) => {
    const limit = +req.query.limit || 3;
    const page = +req.query.page || 1;
    const id = +req.params.id;

    Promise.all([
        models.products.count({ where : { category_id : id}}),
        models.products.findAll({
            where : { category_id : id },
            limit : limit,
            offset : page - 1
        })
        ])
        .then(result => {
            res.status(200)
                .json({
                    products : result[1],
                    total : result[0]
                });
        })
        .catch(err => {
            console.error(err);
            res.status(500)
                .json({
                    message : 'something went wrong'
                })
        })
});

/* GET product listing. */
router.get('/top-products', function(req, res, next) {

  models.products.findAll({
    limit : 3
  })
      .then(products => {
          res.status(200)
              .json({
                products : products
              });
      })
      .catch(err => {
        console.error(err);
        res.status(500)
            .json({
              message : 'something went wrong'
            })
      })
});

module.exports = router;