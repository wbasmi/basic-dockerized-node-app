"use strict";

module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define("products", {
        id : {
            type : DataTypes.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        name : DataTypes.STRING,
        description : DataTypes.STRING,
        price : DataTypes.FLOAT
    }, {
        classMethods: {
            associate: (models) =>{

                Product.belongsTo(models.categories,
                    {
                        as : 'Category',
                        foreignKey : 'category_id',
                        constraints : false
                    });
            }
        }
    });

    return Product;
};