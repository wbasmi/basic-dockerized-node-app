'use strict';

module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define("categories", {
        id : {
            type : DataTypes.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        name : DataTypes.STRING
    }, {
        classMethods : {
            associate : models => {
                Category.hasMany(models.products, {
                    as : 'Products',
                    foreignKey : 'category_id'
                });
            }
        }
    });
    return Category;
};