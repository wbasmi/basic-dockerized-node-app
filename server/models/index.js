"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
const DATABASE = process.env.APP_DB_NAME || 'ecommerce';
const USERNAME = process.env.APP_DB_USERNAME || 'ecommerce';
const PASSWORD = process.env.APP_DB_PWD || 'secret';
const HOST = process.env.APP_DB_HOST || '192.168.99.100';
const PORT = process.env.APP_DB_PORT || 3306;

const sequelize = new Sequelize(DATABASE, USERNAME, PASSWORD, {
    host : HOST,
    port : PORT,
    logging : false
});
var db        = {};

fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function(file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;